provider "aws" {
  region = "ap-south-1"
}
variable vpc_cidr_block {}
variable env_prefix {}
variable subnet_cidr_block {}
variable avai_zone {}
variable my_ip {}
variable my_public_key_location {}
variable my_private_key_location {}
variable my_ami {}
variable my_instance_type {}


resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.vpc_cidr_block
  enable_dns_hostnames = true
  tags = {
    Name: "${var.env_prefix}-vpc"
  }   
}

resource "aws_subnet" "myapp-subnet" {
  vpc_id = aws_vpc.myapp-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.avai_zone
  tags = {
    Name: "${var.env_prefix}-subnet"
  }   
}

resource "aws_internet_gateway" "myapp-igw" {
  vpc_id = aws_vpc.myapp-vpc.id
  tags = {
    Name: "${var.env_prefix}-igw"
  }
}

resource "aws_route_table" "myapp-route-table" {
  vpc_id = aws_vpc.myapp-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
    }
  tags = {
    Name: "${var.env_prefix}-rtb"
  }
}

resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id = aws_subnet.myapp-subnet.id
  route_table_id = aws_route_table.myapp-route-table.id
}

resource "aws_default_security_group" "default-sg" {
  vpc_id = aws_vpc.myapp-vpc.id
  
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8083
    to_port = 8083
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8081
    to_port = 8081
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name: "${var.env_prefix}-sg"
  }   
}

data "aws_ami" "latest-amazon-linux-image" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }
}

resource "aws_key_pair" "server-key" {
  key_name = "server_key"
  public_key = var.my_public_key_location
}

resource "aws_instance" "Server-dev-1" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.my_instance_type

  subnet_id = aws_subnet.myapp-subnet.id
  vpc_security_group_ids = [aws_default_security_group.default-sg.id]
  availability_zone = var.avai_zone

  associate_public_ip_address = true
  key_name = aws_key_pair.server-key.key_name
  
  tags = {
    Name = "Development"
  }
   
}
 
resource "aws_instance" "Server-Prod-1" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.my_instance_type

  subnet_id = aws_subnet.myapp-subnet.id
  vpc_security_group_ids = [aws_default_security_group.default-sg.id]
  availability_zone = var.avai_zone

  associate_public_ip_address = true
  key_name = aws_key_pair.server-key.key_name
  
  tags = {
    Name = "Production"
  }
   
}
 

output "server-dev-1-ip" {
  value = aws_instance.Server-dev-1.public_ip
}

output "server-Prod-1-ip" {
  value = aws_instance.Server-Prod-1.public_ip
}